import React from 'react'
import axios from 'axios'
import PropTypes from 'prop-types'
import Avatar from 'material-ui/Avatar'
import Chip from 'material-ui/Chip'
import LinearProgress from 'material-ui/LinearProgress'
import './table.css'
import logo from '../../assets/images/f1_logo.svg'

export default class DriversByYearComponent extends React.Component {
    constructor (props) {
        super(props)
        this.state = { results: [], year: null, fetchError: null }
    }

    componentWillReceiveProps(newProps) { // Fetching winners by year
        let { results } = this.state
        // Preventing unnecessary API calls and glitch in view
        if (newProps.year && newProps.year !== this.state.year) {
            this.setState({ results: [], year: null })
            axios.get('http://ergast.com/api/f1/' + newProps.year + '/results/1.json')
                .then((response) => {
                    results = response.data.MRData.RaceTable.Races
                    this.setState({ results, year: newProps.year, fetchError: null })
                })
                .catch((error) => {
                    if (!error.response) {
                        this.setState({ fetchError: 'Sorry, Error From The Server' })
                    } else {
                        this.setState({ fetchError: 'Sorry, Error From The Server With Code:' + error.response.status })
                    }
                })
        }
    }

    render() {
        const { results, fetchError } = this.state
        return (
            <div className='divContains'>
                <div style={{ width: '100%' }}>
                    <Chip style={{ margin: '5px 5px' }}>
                        <Avatar src={logo} />
                        {this.state.year}
                    </Chip>
                    <span className='spans'> Winners Of The Season</span>
                </div>
                { results.length > 0 ?
                    <table className='pure-table pure-table-horizontal'>
                        <thead><tr><td>Driver</td><td>Race</td></tr></thead>
                        <tbody>
                            {results.map((race) => {
                                return (
                                    <tr className={race.Results[0].Driver.code === this.props.championCode ? 'fancy' : ''} key={race.date}>
                                        <td> {race.Results[0].Driver.givenName + ' ' + race.Results[0].Driver.familyName}</td>
                                        <td> {race.raceName}</td>
                                    </tr>)
                            })}
                        </tbody>
                    </table>
                    : fetchError ? <span className='error'> {fetchError} </span>
                        : <LinearProgress mode='indeterminate' />}
            </div>
        )
    }
}

DriversByYearComponent.propTypes = {
    year: PropTypes.number,
    championCode: PropTypes.string,
}

DriversByYearComponent.defaultProps = {
    year: null,
    championCode: null
}
