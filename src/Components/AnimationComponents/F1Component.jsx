import React from 'react'
import './f1Styles.css'
import f1Black from '../../assets/images/f1Black.png'

export default class F1Component extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }
    render () {
        return (
            <div id='Container'>
                <img src={f1Black} width='44%' alt='F1' />
            </div>
        )
    }
}
