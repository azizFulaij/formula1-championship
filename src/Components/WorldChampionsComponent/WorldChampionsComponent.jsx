import React, { Component } from 'react'
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import Drawer from 'material-ui/Drawer'
import CircularProgress from 'material-ui/CircularProgress'
import axios from 'axios'
import DriversByYearComponent from '../DriverByYearComponent/DriversByYearComponent'
import F1Component from '../AnimationComponents/F1Component'
import './styles.css'

export default class WorldChampionsComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            open: false,
            champions: [],
            fetchError: null,
        }
        this.handleToggle = this.handleToggle.bind(this)
    }

    componentWillMount() { // Fetching Champions by selected years
        const { champions } = this.state
        const fromSeason = 2005
        const toSeason = 2015

        // Since this is a static data that will not change we will cache it in the browser
        // For two reasons: 1. Faster load & preformance. 2. Minimize hits on server.
        const championsFromLocalStorage = JSON.parse(localStorage.getItem('champs')) || []

        if (championsFromLocalStorage.length > 0) {
            this.setState({ champions: championsFromLocalStorage })
        } else {
            axios.get('http://ergast.com/api/f1/driverStandings/1.json?limit=68')
                .then((response) => {
                    const results = response.data.MRData.StandingsTable.StandingsLists
                    for (const chamption in results) {
                        const season = parseInt(results[chamption].season, 10)
                        if (season >= fromSeason && season <= toSeason) {
                            champions.push(results[chamption])
                        }
                    }
                    localStorage.setItem('champs', JSON.stringify(champions))
                    this.setState({ champions, fetchError: null })
                })
                .catch((error) => {
                    if (!error.response) {
                        this.setState({ fetchError: 'Sorry, Error From The Server' })
                    } else {
                        this.setState({ fetchError: 'Sorry, Error From The Server With Code:' + error.response.status })
                    }
                })
        }
    }

    handleToggle (seasonYear, code) {
        let { year } = this.state
        year = parseInt(seasonYear, 10)
        this.setState({
            open: !this.state.open,
            year,
            championCode: code,
        })
    }

    render() {
        const {
            champions,
            fetchError,
            year,
            championCode
        } = this.state
        return (
            <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
                <div id='wrapper'>
                    <div className='row' id='intro'>
                        <F1Component />
                        <div className='col-sm-8'>
                            <div className='block-reveal'>
                                <div className='animated swipe' data-animation='swipe' />
                                <div className='animated swipe250' data-animation='swipe250' />
                                <h2
                                    className='animated color'
                                    id='introtext'
                                    data-animation='color'
                                >
                                   A React App that shows a list of the F1 world champions.

                                </h2>
                            </div>

                        </div>
                    </div>
                    <section className='links'>
                        <h3 className='year'>Year</h3>
                        <ul>
                            <h3 className='month'>Champion</h3>
                            {champions.length > 0 ?
                                champions.map((chamption) => {
                                    return (
                                        <li key={chamption.season}>
                                            <a
                                                onClick={this.handleToggle.bind(
                                                    this,
                                                    chamption.season,
                                                    chamption.DriverStandings[0].Driver.code
                                                )}
                                            >
                                                <div className='title'>
                                                    <h4>{chamption.season}</h4>
                                                </div>
                                                <div className='date'><span>{chamption.DriverStandings[0].Driver.givenName + ' ' + chamption.DriverStandings[0].Driver.familyName}</span></div>
                                            </a>
                                        </li>
                                    )
                                })
                                : fetchError ? <span className='error'> {fetchError} </span>
                                    : <CircularProgress
                                        style={{ margin: '0 auto', display: 'block' }}
                                        size={80}
                                        thickness={5}
                                        color='#272727'
                                    />}
                        </ul>
                    </section>
                    <Drawer
                        docked={false}
                        width={window.innerWidth > 500 ? 420 : '90%'}
                        openSecondary
                        open={this.state.open}
                        onRequestChange={(open) => { this.setState({ open }) }}
                    >
                        <DriversByYearComponent
                            year={year}
                            championCode={championCode}
                        />
                    </Drawer>
                </div>
            </MuiThemeProvider>
        )
    }
}
