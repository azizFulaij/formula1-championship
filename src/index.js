import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import WorldChampionsComponent from './Components/WorldChampionsComponent/WorldChampionsComponent';

ReactDOM.render(<WorldChampionsComponent />, document.getElementById('root'));
