Formula 1 Championship is a react app

make sure you install packages with: npm install

To run the project simply type: yarn start or npm start 

## Folder Structure

After creation, your project should look like this:

```
formula1-championship/
  README.md
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
  src/
    /assets
      /fonts
      /images
    /Components
      /AnimationComponents
        F1Component.jsx
        f1Styles.css
      DriversByChampionsComponent.jsx
      WorldChampionsComponent.jsx
      styles.css
      table.css
    index.css
    index.js
    logo.svg
```
